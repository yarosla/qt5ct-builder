#!/bin/bash

QT5CT_VERSION=0.34

apt-get update && apt-get install -y build-essential qt5-qmake qtchooser qtbase5-dev libqt5svg5-dev \
    libpng-dev qtbase5-dev-tools qttools5-dev-tools qtbase5-private-dev \
    libmtdev1 libmtdev-dev \
    libglib2.0-dev libxrender-dev libfreetype6-dev libfontconfig1-dev libegl1-mesa-dev \
    curl wget checkinstall

cd /opt

wget -c http://jaist.dl.sourceforge.net/project/qt5ct/qt5ct-$QT5CT_VERSION.tar.bz2 -O qt5ct.tar.bz2 \
  && tar xjf qt5ct.tar.bz2 \
  && cd qt5ct-$QT5CT_VERSION \
  && QT_SELECT=5 qmake PREFIX=/usr/local \
  && make -j 4 \
  && mkdir /out \
  && checkinstall --pakdir /out

echo "Uploading to: ${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}..."
curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"/out/qt5ct_${QT5CT_VERSION}-1_amd64.deb"

# qt5ct builder for Debian Stretch

This project contains build pipeline (feature of Bitbucket)
to build from source **qt5ct** `.deb` package.

See build script in `build.sh`. Feel free to fork this repo
to build **qt5ct** for other Linux distributions.

# About qt5ct

Qt5 Configuration Tool

This program allows users to configure Qt5 settings (theme, font, icons, etc.)
under DE/WM without Qt integration.

- [Home page](https://sourceforge.net/projects/qt5ct/)
- [Source repository](https://github.com/mati75/qt5ct)

Useful for running KDE5/Qt5 programs under Gnome/Xfce/... desktop.
See [krusader case](https://pantas.net/krusader_kde5/) for example.

# Usage

Download `.deb` package from [Downloads](https://bitbucket.org/yarosla/qt5ct-builder/downloads/) page.

    sudo dpkg -i qt5ct_0.34-1_amd64.deb
    KDE_SESSION_VERSION=5 \
      KDE_FULL_SESSION=true \
      QT_QPA_PLATFORMTHEME=qt5ct \
      qt5ct

Don't forget to set the following environment variables
so that qt5ct is activated for your apps:

    export KDE_SESSION_VERSION=5
    export KDE_FULL_SESSION=true
    export QT_QPA_PLATFORMTHEME=qt5ct
